import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'

import { ROUTES } from './app-routing.module'

import { AppComponent } from './app.component'

import { HomeComponent } from './home/app.home'

import { HeaderComponent } from './layout/header/app.header'
import { FooterComponent } from './layout/footer/app.footer'

import { NewsComponent } from './news/app.news'
import { IndicatorsComponent } from './indicators/app.indicators'
import { FormationMaterialsComponent } from './formation-materials/app.formation-materials'

import { AboutComponent } from './about/app.about'
import { HelpComponent } from './help/app.help'


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES),
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    AboutComponent,
    HelpComponent,
    NewsComponent,
    IndicatorsComponent,
    FormationMaterialsComponent
  ],  
  providers: [
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }
