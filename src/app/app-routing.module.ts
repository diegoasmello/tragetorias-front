import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { HomeComponent } from './home/app.home'

import { AboutComponent } from './about/app.about'
import { HelpComponent } from './help/app.help'
import { NewsComponent } from './news/app.news'
import { IndicatorsComponent } from './indicators/app.indicators'
import { FormationMaterialsComponent } from './formation-materials/app.formation-materials'

export const ROUTES: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'noticias', component: NewsComponent },
    { path: 'indicadores', component: IndicatorsComponent },
    { path: 'formacao', component: FormationMaterialsComponent },
    { path: 'sobre', component: AboutComponent },
    { path: 'ajuda', component: HelpComponent }
]
