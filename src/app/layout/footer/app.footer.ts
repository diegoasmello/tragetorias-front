import { Component } from '@angular/core';

@Component({
    selector: 'my-footer',
    template: `<footer id="footer" class="main-footer rodape-gov">
    <div class="faixa m-0"></div>
    <div class="row m-0 text-center">
      <div class="col-md-2">
        <a alt="Acesso à informação" href="http://www.acessoainformacao.gov.br/" target="_blank">
          <img src="img/acesso-a-informacao.png">
        </a>
      </div>
      <div class="col-md-8">
        <ul class="list-inline links">
          <li>2017 | <a href="http://ufsc.br/" target="_blank">NUTE-UFSC</a> | </li>
          <li><a href="http://www.mec.gov.br/" target="_blank">MEC</a>. Todos os direitos reservados.</li>
        </ul>
      </div>
      <div class="col-md-2">
        <a alt="Governo Federal" href="http://www.brasil.gov.br/" target="_blank">
          <img src="img/governo-federal.png">
        </a>
      </div>
    </div>
  </footer>`,
})

export class FooterComponent { }
