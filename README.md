# Hello, and welcome!!!

## To start this project, follow the steps below.

1. Grab a coffee

2. Clone this repository 
```
$ git clone https://codigos.ufsc.br/nute/tragetorias-front.git
```

3. After clone, in the project file
```
$ npm install
$ npm start
```

5. Save some file to gulp do his job, gulp will minify and concatenate some files and put in assets folfer

6. You are in branch **master**, go to branch **dev**
```
$ git checkout dev
```

7. Open a new branch for your work from branch **dev**
```
$ git checkout -b your-branch
```

8. Now you are free to code

9. When you finish your work, commit and push
```
$ git add -A or git add .
$ git commit -m 'what you do'
$ git push origin your-branch
```

10. If everything is ok, now you can merge your branch to branch **dev**, but before merge you need to know if **dev** has something you don't have
```
$ git checkout dev
$ git fetch
$ git rebase
$ git checkout your-branch
$ git rebase dev
```
If has conflict, you are alone in this world, sorry, is your fuck**** problem, not my.
Resolve the conflict and keep going.

11. Now you can merge to **dev**, go back to branch **dev**
```
$ git chekout dev
$ git merge your-branch
$ git push origin dev
```
12. Hell yeah, you done it!

13. Go back to step 7 and grab a coffee


> **Tips**

 - Always check what you have done with:
```
$ git status
``` 

 - To save your work without erasing everything:
```
$ git stash
$ git stash pop (To get back what you did)
```

 - You can see all the commits with:
```
$ gitk
```
